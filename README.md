# Pico UDP
This is a very simple setup to visualize IoT Data on Dashboards, to work with Urban Data and to get familiar with some concepts behind Urban Data Platforms (UDP) like FUTR-HUB, UDSP and others.

Just use it as a Playgound, not for Production use!

## Getting started
### Setting up Pico UDP
1. Install docker & docker compose plugin
3. Clone this repo
4. Inside this directory execute: `docker compose up -d` to run all the docker containers.

### Stop Pico UDP
1. `docker compose down` Attention! All Data will be lost, if you don't use persistent volumes!

### Update Pico UDP
1. run `git pull` inside this directory to update the main repo


### Services
- Node-Red: Dataflowprogramming/ETL System. http://localhost:1880/
- Grafana: Dashboard-Tool. http://localhost:3000/
    - username: admin
    - password: admin
- mysql: Database. localhost:3306 
- PhpMyAdmin: Databasemanagement-Tool. http://localhost:8080/
    - Server: mysql
    - Database: sensors 
    - username: root
    - password: pass.1234
